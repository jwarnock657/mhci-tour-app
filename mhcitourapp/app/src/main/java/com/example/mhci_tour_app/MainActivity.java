package com.example.mhci_tour_app;

import androidx.fragment.app.FragmentActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private boolean isFABOpen;
    private FloatingActionButton fab2;
    private FloatingActionButton fab3;
    private Button quizBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        isFABOpen = false;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) findViewById(R.id.fab3);



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Awards.class);
                startActivity(intent);
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProfilePage.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        float zoom = 15.0f;
        final Context context = this;

        addMarkers();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(55.872549, -4.288262)));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(zoom));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                try {
                    String title = marker.getTitle();
                    showDialog(context, title);
                }
                catch (Exception e){
                    Log.d( "Uh oh spaghettio", e.toString());
                }
                return false;
            }

            public void showDialog(Context context, String title){
                LayoutInflater factory = LayoutInflater.from(context);
                final View view;

                Log.d("TITLE", title);
                Log.d("LEN", Integer.toString(title.length()));

                if(title.length() == 18){
                    view = factory.inflate(R.layout.main_gate, null);
                }
                else if(title.length() == 17){
                    view = factory.inflate(R.layout.boyd_orr, null);
                }
                else{
                    view = factory.inflate(R.layout.macmillan, null);
                }

                new AlertDialog.Builder(context)
                        .setTitle(title)
                        .setView(view)

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton("Close", null)
                        .show();


                quizBtn = (Button) view.findViewById(R.id.quiz);

                quizBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, QuizEnterActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    public void addMarkers() {
        Log.d("Markers", "Adding markers to map");
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.871574, -4.289412)).title("University Chapel"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.872294, -4.288157)).title("The Memorial Gates").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.871805, -4.290123)).title("Professors Square"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.873694, -4.293700)).title("Boyd Orr Building"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.870796, -4.288789)).title("University Flag Pole"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.873294, -4.288429)).title("University Library"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.871519, -4.288449)).title("University Cloisters"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.871724, -4.288302)).title("Hunterian Museum"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.872898, -4.288763)).title("The Door"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.872688, -4.292094)).title("The Botany Gate"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.873679, -4.291405)).title("QMU"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.872631, -4.284708)).title("GUU"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.872679, -4.288023)).title("MacMillan Round Reading Room"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(55.872954, -4.293000)).title("Wolfson Medical Building"));
    }

    private void showFABMenu(){
        isFABOpen=true;
        fab2.animate().translationY(-getResources().getDimension(R.dimen.standard_60));
        fab3.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
    }

    private void closeFABMenu(){
        isFABOpen=false;
        fab2.animate().translationY(0);
        fab3.animate().translationY(0);
    }


}
