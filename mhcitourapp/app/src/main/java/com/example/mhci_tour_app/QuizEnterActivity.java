package com.example.mhci_tour_app;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class QuizEnterActivity extends AppCompatActivity {

    private static final long COUNTDOWN_IN_MILLIS = 12000;
    private ColorStateList textColorDefaultCd;
    private CountDownTimer countDownTimer;
    private long timeLeftInMillis;
    private TextView textViewCountDown;
    private Button joinButton;
    private boolean start = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_enter);

        TextView textView = (TextView) findViewById(R.id.POITitleText2);
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        Button buttonStartQuiz = findViewById(R.id.button_start_join);
        buttonStartQuiz.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startQuiz();
            }
        });

        textViewCountDown = findViewById(R.id.text_test_countdown2);
        joinButton = findViewById(R.id.button_start_join);

        textColorDefaultCd = textViewCountDown.getTextColors();
        timeLeftInMillis = COUNTDOWN_IN_MILLIS;
        startCountDown();

    }

    private void startCountDown(){
        countDownTimer = new CountDownTimer(timeLeftInMillis, 1000) {
            @Override
            public void onTick(long l) {
                timeLeftInMillis = l;
                updateCountDownText();
            }

            @Override
            public void onFinish() {
                timeLeftInMillis = 0;
                updateCountDownText();
                if(!start){
                    startQuiz();
                }
            }
        }.start();
    }

    private void updateCountDownText(){
        int minutes = (int) (timeLeftInMillis / 1000) / 60;
        int seconds = (int) (timeLeftInMillis / 1000) % 60;

        String timeFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        textViewCountDown.setText(timeFormatted);

        if (timeLeftInMillis < 10000){
            textViewCountDown.setTextColor(Color.RED);
        } else {
            textViewCountDown.setTextColor(textColorDefaultCd);
        }
        if (timeLeftInMillis < 8000){
            joinButton.setAlpha((float) 1);

        } else {
            joinButton.setAlpha((float) 0.5);

        }
    }


    private void startQuiz() {
        Handler handler = new Handler();
        start = true;
        Log.d("startQuiz", "Pressed Button");
        Intent intent = new Intent(QuizEnterActivity.this, QuizPage.class);
        startActivity(intent);
        handler.postDelayed(new Runnable() {
            public void run() {
//                textViewCountDown.setVisibility(View.INVISIBLE);
//                congratsText.setVisibility(View.VISIBLE);
                textViewCountDown.setTextColor(Color.WHITE);
                textViewCountDown.setText("Congrats you got 3 right!");
            }
        }, 1000);

    }
}