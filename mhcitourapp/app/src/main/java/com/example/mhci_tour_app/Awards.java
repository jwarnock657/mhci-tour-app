package com.example.mhci_tour_app;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Awards extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_awards);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void imageClick(View v) {
        switch (v.getId()) {
            case (R.id.thirdTotal):
                displayToast(v,"You have 5 bronze medals");
                break;
            case (R.id.secondTotal):
                displayToast(v,"You have 8 silver medals");
                break;
            case (R.id.firstTotal):
                displayToast(v,"You have 9 gold medals");
                break;
            case (R.id.boyd):
                displayToast(v,"Boyd Orr Building: 1 medal");
                break;
            case (R.id.library):
                displayToast(v,"University Library: 3 medals");
                break;
            case (R.id.jo_black):
                displayToast(v,"Joseph Black Building: 5 medals");
                break;
            case (R.id.wolfson):
                displayToast(v,"Wolfson Medical Building: 0 medals");
                break;
            case (R.id.sawb):
                displayToast(v,"Sir Alwyn Williams Building: 6 medals");
                break;
            case (R.id.adamsmith):
                displayToast(v,"Adam Smith Building: 0 medals");
                break;
            case (R.id.kelvin):
                displayToast(v,"Kelvin way: 1 medal");
                break;
            case (R.id.chapal):
                displayToast(v,"University Chapel: 3 medals");
                break;
            case (R.id.readingroom):
                displayToast(v,"Reading Room: 1 medal");
                break;
            case (R.id.jameswatt):
                displayToast(v,"James Watt Building: 2 medals");
                break;
            case (R.id.hillhead):
                displayToast(v,"Hillhead Subway Station: 0 medals");
                break;
        }
    }

    public void displayToast(View v, String text) {
        Toast toast = Toast.makeText(Awards.this, text, Toast.LENGTH_SHORT );
        View toastView = toast.getView();
        toastView.setBackgroundColor(getResources().getColor(R.color.uniAccent));

        TextView dv = (TextView) toast.getView().findViewById(android.R.id.message);
        dv.setTextColor(Color.WHITE);
//        toastView.setBackgroundColor(getResources().getColor(R.color.uniAccent));

        toast.show();

    }
}
