package com.example.mhci_tour_app;

import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ProfilePage extends AppCompatActivity implements View.OnClickListener{

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }

    private Button btn;
    private Button btn2;
    private Button btn3;
    private TextView coins;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);

        btn = (Button) findViewById(R.id.buyItem1);
        btn.setOnClickListener(this);

        btn2 = (Button) findViewById(R.id.buyItem2);
        btn.setOnClickListener(this);

        btn3 = (Button) findViewById(R.id.buyItem3);
        btn.setOnClickListener(this);

        coins = (TextView) findViewById(R.id.coins);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buyItem1) {
            btn.setText("Bought");
            displayToast(v, "You redeemed a KeepCup!");
        }else if(v.getId() == R.id.buyItem2){
            btn.setText("Bought");
        }else if(v.getId() == R.id.buyItem3){
            btn.setText("Bought");
        }

        coins.setText("Coins: 1000");
    }

    public void displayToast(View v, String text) {
        Toast toast = Toast.makeText(ProfilePage.this, text, Toast.LENGTH_SHORT );
        View toastView = toast.getView();
        toastView.setBackgroundColor(getResources().getColor(R.color.uniAccent));

        TextView dv = (TextView) toast.getView().findViewById(android.R.id.message);
        dv.setTextColor(Color.WHITE);
//        toastView.setBackgroundColor(getResources().getColor(R.color.uniAccent));

        toast.show();

    }
}
